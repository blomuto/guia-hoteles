$(function(){
    /* tooltip y popover */
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
});

/* carousel */
$('.carousel').carousel({
    interval: 3000
});

/* modal */
$('#contacto').on('show.bs.modal', function(e){
  console.log('el modal se esta mostrando');

  $('#contactoBtn').removeClass('btn-outline-success');
  $('#contactoBtn').addClass('btn-primary');
  $('#contactoBtn').prop('disabled', true);
});

$('#contacto').on('hidden.bs.modal', function(e){
  console.log('el modal se oculto');

  $('#contactoBtn').prop('disabled', false);
  $('#contactoBtn').removeClass('btn-primary');
  $('#contactoBtn').addClass('btn-outline-success');
});